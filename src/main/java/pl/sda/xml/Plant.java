package pl.sda.xml;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import java.math.BigInteger;
import java.util.List;

@XmlAccessorType(XmlAccessType.FIELD)
public class Plant {

    /**
     * <common>phlox, blue</common>
     * <botanical>phlox divaricata</botanical>
     * <zone>3</zone>
     * <lights>
     * <light>sun</light>
     * <light>shade</light>
     * </lights>
     * <price>$5.59</price>
     * <availability>021699</availability>
     */

    @XmlAttribute(name = "inShop")
    private String inShop;
    @XmlElement(name = "COMMON_NAME")
    private String common;
    private String botanical;
    private int zone;
    private List<Light> lights;
    private String price;
    private BigInteger availability;

    public String getInShop() {
        return inShop;
    }

    public void setInShop(String inShop) {
        this.inShop = inShop;
    }


    public String getCommon() {
        return common;
    }

    public void setCommon(String common) {
        this.common = common;
    }

    public String getBotanical() {
        return botanical;
    }

    public void setBotanical(String botanical) {
        this.botanical = botanical;
    }

    public int getZone() {
        return zone;
    }

    public void setZone(int zone) {
        this.zone = zone;
    }

    public List<Light> getLights() {
        return lights;
    }

    public void setLights(List<Light> lights) {
        this.lights = lights;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public BigInteger getAvailability() {
        return availability;
    }

    public void setAvailability(BigInteger availability) {
        this.availability = availability;
    }
}
