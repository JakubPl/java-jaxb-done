package pl.sda.xml;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class Main {
    public static void main(String[] args) throws JAXBException {
        //orginalny XML pochodzi z https://www.w3schools.com/xml/plant_catalog.xml

        final InputStream plantCatalogInputStream = Main.class.getClassLoader().getResourceAsStream("plant_catalog.xml");
        JAXBContext jaxbContext = JAXBContext.newInstance( Catalog.class );
        Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
        final Catalog catalog = (Catalog) jaxbUnmarshaller.unmarshal(plantCatalogInputStream);
        System.out.println(catalog.getPlants().size());


        final Plant plant = new Plant();
        plant.setCommon("cn");
        catalog.getPlants().add(plant);
        final Marshaller marshaller = jaxbContext.createMarshaller();
        marshaller.marshal(catalog, new OutputStream() {
            @Override
            public void write(int b) throws IOException {
                System.out.print((char) b);
            }
        });

    }
}
